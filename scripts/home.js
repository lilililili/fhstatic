function email_click() {
	field = $('#email');
	if (field.val() == 'enter your email address') {
		field.val('');	
	}
}

function email_submit() {
	field = $('#email');
	email_click();
	if (!field.val()) {
		alert('Please enter your email address');
		field.focus();
		return;
	}
	if (!isValidEmail(field.val())) {
		alert('Please enter a valid email address');
		field.focus();
		return;
	}
	$('#email_form').css('display','none');
	$('#email_saving').css('display','');
	$.ajax({'data':{"email":field.val()},'dataType':'html','type':'POST','url':'/ajax.asp?action_type=join_mailing_list','success':function(data,textStatus) {
		$('#email_saving').html(data);
	}});
	
}

function isValidEmail(email){return(email.indexOf('.')>0)&&(email.indexOf('@')>0);}
function on_enter_do_function(e,func){if(e.keyCode==13){func();}}